import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import os
from skimage.measure import ransac
from skimage.transform import ProjectiveTransform, AffineTransform

def draw_img(img):
    plt.figure(figsize=(15, 20))
    plt.imshow(cv.cvtColor(img,cv.COLOR_BGR2RGB))

def get_sift_on_image(img,plot = False):
    '''
    Извлечение признаков методом SIFT,
    Если plot = True, рисуется изображение с признаками
    res - изображение с признаками, des - полученные дескрипторы
    kp - ключевые точки
    '''
    gray_img = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    sift = cv.SIFT_create()
    kp, des = sift.detectAndCompute(img,None)
    res = cv.drawKeypoints(gray_img,kp,img,flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    if plot:
        plt.figure(figsize=(10, 15))
        plt.imshow(img)
        
    return res,kp, des

def get_all_jpg_from_dir(path):
    '''
    Считывание всех jpg файлов с заданной директории
    '''
    dataset = []
    fds = sorted(os.listdir(path))
    for img in fds:
        if img[-4:].lower() == '.jpg':
            print('/'.join([path,img]))
            dataset.append(cv.imread('/'.join([path,img])))
    return dataset

def draw_img_with_features(img,kp):
    res = cv.drawKeypoints(cv.cvtColor(img,cv.COLOR_BGR2GRAY),kp,img,flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    plt.figure(figsize=(10, 15))
    plt.imshow(res)

def keypoints_matching(img1, kp1, des1, img2, kp2, des2,plot=False,threshold=8,trees=5,min_samples=4):
    '''
    Сопоставление изображений методами FLANN и RANSAC
    img1,kp1,des1 - изображение c помеченными ключевыми точками, 
    ключевые точки и дескрипторы полученные методом SIFT для исходного изображения (на котором ищем)
    img2,kp2,des2 - аналогично img1,kp1,des1, только для изображения, которое мы ищем
    plot - определяет, нужно ли строить график
    threshold - пороговый параметр RANSAC
    min_samples - минимальное количество объектов для построения зависимости в RANSAC
    trees - количество деревьев в FLANN
    '''
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = trees)
    search_params = dict(checks=50)   # or pass empty dictionary
    flann = cv.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1,des2,k=2)

    # ratio test as per Lowe's paper
    good = []
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.7*n.distance:
            good.append(m)
            
    # Координаты сопоставленных точек на исходном изображении            
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 2)  
    # Координаты сопоставленных точек на изображении, точки которого мы пытаемся определить на исходном
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 2)

    #Ransac
    model, inliers = ransac(
            (src_pts, dst_pts),
            AffineTransform, min_samples=min_samples,
            residual_threshold=threshold, max_trials=10
        )
    
    n_inliers = np.sum(inliers)
    
    inlier_keypoints_left = [cv.KeyPoint(point[0], point[1], 1) for point in src_pts[inliers]]
    inlier_keypoints_right = [cv.KeyPoint(point[0], point[1], 1) for point in dst_pts[inliers]]
    placeholder_matches = [cv.DMatch(idx, idx, 1) for idx in range(n_inliers)]
    
    if plot:
        draw_params = dict(matchColor = (0,255,0),
                       singlePointColor = (255,0,0),
                       flags = cv.DrawMatchesFlags_DEFAULT)
        
        image3 = cv.drawMatches(img1, inlier_keypoints_left, img2, inlier_keypoints_right, placeholder_matches, None,**draw_params)
        
        plt.figure(figsize=(50, 100))
        plt.imshow(image3,),plt.show()
    
    src_pts = np.float32([ inlier_keypoints_left[m.queryIdx].pt for m in placeholder_matches ]).reshape(-1, 2)
    dst_pts = np.float32([ inlier_keypoints_right[m.trainIdx].pt for m in placeholder_matches ]).reshape(-1, 2)
    
    return src_pts, dst_pts

def mat2gray(img):
    A = np.double(img)
    out = np.zeros(A.shape, np.double)
    normalized = cv.normalize(A, out, 1.0, 0.0, cv.NORM_MINMAX)
    return out
 
def random_noise(image, mode='gaussian', seed=None, clip=True, **kwargs):
    '''
    Наложение шума на изобраение
    в качестве kwargs передаются 2 значение:
    mean - среднее значение для шума, var - дисперсия
    '''
    image = mat2gray(image)
    
    mode = mode.lower()
    if image.min() < 0:
        low_clip = -1
    else:
        low_clip = 0
    if seed is not None:
        np.random.seed(seed=seed)
        
    if mode == 'gaussian':
        noise = np.random.normal(kwargs['mean'], kwargs['var'] ** 0.5,
                                 image.shape)        
        out = image  + noise
    if clip:        
        out = np.clip(out, low_clip, 1.0)
        
    return np.uint8(out*255)
